package de.academy.jee.webblog.services;


import de.academy.jee.webblog.entities.Post;
import de.academy.jee.webblog.model.PostModel;
import de.academy.jee.webblog.repository_datenBankZugriff.PostRepository;


import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.transaction.Transactional;

import java.util.ArrayList;
import java.util.List;

//ApplicationScope
@Stateless
public class PostServiceBean {


    @EJB // Injeziere mein Repository mit Ansammlung von Queries
    private PostRepository postRepository;


    public PostServiceBean() {
    }


    @Transactional
    public void createAndSafePost(PostModel postModel) {

        Post safePost = new Post(postModel.getTitle(), postModel.getContent());
        postRepository.safeToDB(safePost);

    }


    public List<PostModel> readAllPosts() {

        // Read all post entities from database first
        List<Post> posts = postRepository.readAll();

        // Map all post entities to models to return and digest them on the page.
        List<PostModel> postModels = new ArrayList<>();
        for (Post post : posts) {
            PostModel postModel = new PostModel();
            postModel.setTitle(post.getTitle());
            postModel.setContent(post.getContent());

            postModel.setId(post.getId());
            // Beim Erstellen eines Posts wird die ID von einem PostModel gleichgesetzt
            // Post.ID ist vor der Transaktion null, deswegen ID erst danach setzen

            postModels.add(postModel);

        }
        return postModels;

    }

    public void removeLastPost() {

        //Erstelle Query, Lass mir die komplette Liste anzeigen, speicher in Query
        // Falls Einträge vorhanden dann führe aus
        List<Post> posts = postRepository.readAll();
        if (posts.size() > 0) {
            Post lastPost = posts.get(posts.size() - 1);
            Long lastPostId = lastPost.getId();

            // Finde in meiner Datenbank in der Spalte Post, die ID (Primarykey)
            Post deletePost = postRepository.readEntityByID(lastPostId);

            // Lösche den Eintrag
            postRepository.removeFromDB(deletePost);
        }

    }


    public void deleteThisPost(Long postID) {

        Post deletePost = postRepository.readEntityByID(postID);

        postRepository.removeFromDB(deletePost);
    }


//    @Transactional
//    public void updatePost(Long postId, String newTitle, String newContent) {
//        // --- --- //
//        List<Post> posts = postRepository.readAll();
//        Post upPost = entityManager.find(Post.class, postId);
//        upPost.setTitle(newTitle);
//        upPost.setContent(newContent);
//    }


}
