package de.academy.jee.webblog.services;


import de.academy.jee.webblog.entities.Comment;
import de.academy.jee.webblog.model.CommentModel;
import de.academy.jee.webblog.repository_datenBankZugriff.CommentRepository;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class CommentServiceBean {

    @EJB
    private CommentRepository commentRepository;

    public CommentServiceBean() {
    }

    @Transactional
    public void createAndSafeComment(CommentModel commentModel) {
        Comment comment = new Comment(commentModel.getContent());
        commentRepository.safeToDB(comment);
    }

    public List<CommentModel> readAllComments() {

        List<Comment> comments = commentRepository.readAll();

        //Map all comment entities to models to return and digest them on the page.
        List<CommentModel> commentModels = new ArrayList<>();
        for (Comment comment : comments) {
            CommentModel commentModel = new CommentModel();
            commentModel.setContent(comment.getContent());

            commentModel.setCommentId(comment.getId());
            // Beim Erstellen eines Kommentars wird die ID von einem CommentModel gleichgesetzt

        }
        return commentModels;

    }

    public void deleteThisComment(Long commentId){
        Comment deleteComment = commentRepository.readEntityByID(commentId);
        commentRepository.removeFromDB(deleteComment);
    }


}
