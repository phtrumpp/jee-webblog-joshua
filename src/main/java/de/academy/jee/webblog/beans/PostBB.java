package de.academy.jee.webblog.beans;

import de.academy.jee.webblog.model.PostModel;
import de.academy.jee.webblog.services.PostServiceBean;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Named
@RequestScoped
public class PostBB {

    @EJB
    private PostServiceBean postServiceBean;

    private PostModel postModel;

    public PostModel getPostModel() {
        return postModel;
    }

    private String title;
    private String content;

    @PostConstruct // Wird direkt nach dem Kontruktor aufgerufen
    private void init(){
        this.postModel = new PostModel(this.title,this.content);
    }

    public void createPost() {
        postServiceBean.createAndSafePost(postModel);
    }

    public List<PostModel> readAll() {
        //List<PostModel> resultListView = new ArrayList<>();
        return postServiceBean.readAllPosts();
       // return resultListView;
    }

    public void deleteLastPost(){
        postServiceBean.removeLastPost();
    }

    public void deletePost(Long postId){
        postServiceBean.deleteThisPost(postId);
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }


}
