package de.academy.jee.webblog.beans;


import de.academy.jee.webblog.model.CommentModel;

import de.academy.jee.webblog.services.CommentServiceBean;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Named
@RequestScoped
public class CommentBB {

    @EJB
    private CommentServiceBean commentServiceBean;

    private CommentModel commentModel;

    private String content;

    private void init(CommentModel commentModel){

        this.commentModel = new CommentModel(this.content);

    }

    public void createComment() {
        commentServiceBean.createAndSafeComment(commentModel);
    }


    //ToDO remember about the 1 - n relation of Posts and Commments



}
