package de.academy.jee.webblog.beans;


import de.academy.jee.webblog.services.PostServiceBean;

import javax.ejb.EJB;


public class StartBean {

    /**
     * default constructor to prefill some example data.
     */

    @EJB
    private PostServiceBean postServiceBean;
    // Sagt StartBean, dass wir die PostStatefulBean in den Kontext stellen
    // Ohne die Annotation wäre dies einfach nur ein Attribut und Dependency Injection könnte nicht funktionieren
    // Bzw. würde wie ein normales Objekt behandelt werden
    // Beim Serverstart wird dadurch eine Bean erzeugt,
    // welche im Kontext wartet (Ohne Variabel Namen etc., deshalb der explizite Aufruf mit der Annotation

    public StartBean(){


        // --- Demo Konstruktor um Posts zu generieren --- //
//        PostModel post1 = new PostModel();
//        post1.setFliessText("Lalalalalalala - von post1");
//        post1.setTitle("Ich bin 1");
//        //posts.add(post1); ---> Überbleibsel als List<Post> noch in dieser Klasse war
//
//        PostModel post2 = new PostModel();
//        post2.setFliessText("mamnanananana - von post2");
//        post2.setTitle("Ich bin 2");
//        //posts.add(post2);
//
//        PostModel post3 = new PostModel();
//        post3.setFliessText("chahahahahaha - von post3");
//        post3.setTitle("Ich bin 3");
//        // posts.add(post3);
    }


    private static final String PAGE_TITLE = "H1 Tag Überschrift";


    public String getPageTitle() {
        return PAGE_TITLE;
    }

//    public List<PostModel> getPosts() {
//        return postStatefulBean.getPosts();
//    }



}
