package de.academy.jee.webblog.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Comment {

    // Contructors
    public Comment() {

    }

    public Comment(String content) {
        this.content = content;
    }


    @Id
    @GeneratedValue
    private Long id;

    private String content;


    // Getter und Setter

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String inhalt) {
        this.content = inhalt;
    }
}
