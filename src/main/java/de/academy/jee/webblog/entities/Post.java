package de.academy.jee.webblog.entities;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Post {


    // Konstruktoren - wichtig: Entities benötigen immer einen default Konstruktor
    public Post() {
    }

    public Post(String title, String content) {
        this.title = title;
        this.content = content;
    }

    @Id
    @GeneratedValue
    private Long id;

    //private List<Comment> comments;

    @OneToMany
    @JoinColumn
    private List<Comment> comments = new ArrayList<>(); // 1 zu n oder generell Beziehungen von einer Entität zu einer anderen
    // müssen immer mit einer entsprechenden Annotation deklariert werden


    private String content;

    @Column(nullable = false)
    private String title;


    /**
     * Getter und Setter
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getTitle() {
        return title;
    }


    public String getContent() {
        return content;
    }


    public List<Comment> getComments() {
        return comments;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
