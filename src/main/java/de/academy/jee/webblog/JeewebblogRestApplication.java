package de.academy.jee.webblog;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *Einstiegspunkt
 */
@ApplicationPath("/data") // generierte Main-Methode in dieser Annotation und noch mehr
@ApplicationScoped
public class JeewebblogRestApplication extends Application {
}
