package de.academy.jee.webblog.model;

import de.academy.jee.webblog.entities.Comment;

import java.util.ArrayList;
import java.util.List;

public class CommentModel {

    private Long commentId;
    private String content;

    private List<Comment> comments = new ArrayList<>();

    public CommentModel() {
    }


    public List<Comment> getComments() {
        return comments;
    }

    // Brauche ich den Setter hierfür?
    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public CommentModel(String content) {
        this.content = content;
    }

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
