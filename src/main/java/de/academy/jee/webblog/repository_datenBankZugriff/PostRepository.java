package de.academy.jee.webblog.repository_datenBankZugriff;

import de.academy.jee.webblog.entities.Post;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class PostRepository extends Repository<Post> {


    public PostRepository() {
        super(Post.class);
    }



    @Override
    public void safeToDB(Post safeEntity) {
        super.safeToDB(safeEntity);
    }

    @Override
    public List<Post> readAll() {
        return super.readAll();
    }

    @Override
    public Post readEntityByID(Long commentId) {
        return super.readEntityByID(commentId);
    }

    @Override
    public void removeFromDB(Post removeEntity) {
        super.removeFromDB(removeEntity);
    }
}
