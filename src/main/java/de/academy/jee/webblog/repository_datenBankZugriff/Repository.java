package de.academy.jee.webblog.repository_datenBankZugriff;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

/**
 * Author: Philippe Trumpp-Kallmeyer
 * @param <T>
 */
public abstract class Repository<T> {

    private EntityManager entityManager;
    //Look at constructor for usage
    private Class<T> entityClass;


    /**
     * Creates an entityManager for the common Queries
     *
     * @param entityClass e.g. gives us later a reference of the concrete class to get the Class Name (simpleName()) as a String for queries
     */
    public Repository(Class<T> entityClass) {

        EntityManagerFactory entityManagerFactory
                = Persistence.createEntityManagerFactory("JPA_WEBBLOG_PERSISTENCE_UNIT");
        this.entityManager = entityManagerFactory.createEntityManager();

        this.entityClass = entityClass;
    }

    /**
     * Create function to safe entities into the database
     *
     * @param safeEntity Entity you want to safe to the database
     */
    public void safeToDB(T safeEntity) {
        entityManager.getTransaction().begin();
        entityManager.persist(safeEntity);
        entityManager.getTransaction().commit();
    }

    /**
     * Select function which returns a list of entities
     *
     * @return Simple list of entities
     */
    public List<T> readAll() {
        return entityManager.createQuery("select q from " + entityClass.getSimpleName() + " q", entityClass).getResultList();
    } // Verursacht Nullpointer Exception

    /**
     * Select function to find an entity by its ID
     * @param commentId give ID of your comment
     * @return returns the required entity
     */
    public T readEntityByID(Long commentId) {
        T entity = entityManager.find(entityClass, commentId);
        return entity;
    }

    /**
     * Delete function for the given entity
     *
     * @param removeEntity Entity type you want to delete
     */
    public void removeFromDB(T removeEntity) {
        entityManager.getTransaction().begin();
        entityManager.remove(removeEntity);
        entityManager.getTransaction().commit();
    }

    /* TODO - Add Update function
    @Transactional
    public void updateEntity(Long entityId, String newTitle, String newContent) {
        // --- --- //
        List<Post> posts = postRepository.readAll();
        Post upPost = entityManager.find(Post.class, postId);
        upPost.setTitle(newTitle);
        upPost.setContent(newContent);
    }
     */

}
