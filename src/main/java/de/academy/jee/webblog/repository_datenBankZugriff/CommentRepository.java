package de.academy.jee.webblog.repository_datenBankZugriff;

import de.academy.jee.webblog.entities.Comment;


import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class CommentRepository extends Repository <Comment> {


    public CommentRepository() {
        super(Comment.class);
    }


    @Override
    public void safeToDB(Comment safeEntity) {
        super.safeToDB(safeEntity);
    }

    @Override
    public List<Comment> readAll() {
        return super.readAll();
    }

    @Override
    public Comment readEntityByID(Long commentId) {
        return super.readEntityByID(commentId);
    }

    @Override
    public void removeFromDB(Comment removeEntity) {
        super.removeFromDB(removeEntity);
    }
}
